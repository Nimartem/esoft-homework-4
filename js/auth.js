const login = document.querySelector('input[name="login"]');
const password = document.querySelector('input[name="password"]');

login.addEventListener('keydown', inputValidator);
password.addEventListener('keydown', inputValidator);

function inputValidator(e) {
    const typedCharacter = e.key;
    if (!((typedCharacter >= 'A' && typedCharacter <= 'Z') || 
           (typedCharacter >= 'a' && typedCharacter <= 'z') || 
           (typedCharacter >= '0' && typedCharacter <= '9') ||
           (typedCharacter === '.') || (typedCharacter === '_'))) {
            e.preventDefault();
           };
};

login.addEventListener('copy', (e) => e.preventDefault());
password.addEventListener('copy', (e) => e.preventDefault());

document.querySelector('button').addEventListener('click', (e) => console.log(`Логин: ${login.value}, пароль: ${password.value}`));